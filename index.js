const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mysql = require("mysql");
const crypto = require("crypto");
var session = require("express-session");
var MySQLStore = require("express-mysql-session")(session);
const sanitizeHtml = require("sanitize-html");
const multer = require('multer');
const fs = require('fs');

app.use(express.static(__dirname + "/public"));
app.use(session({
    key: "session_cookie_name",
    secret: "session_cookie_secret",
    store: new MySQLStore({
        host:"127.0.0.1",
        port:3306,
        user:"root",
        password:"vajicko",
        database:"cookie_user"
    }),
    resave: false,
    saveUninitialized: false,
    cookie:{
        maxAge:1000*60*60*24,
    }
}));
app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.set("view engine", "ejs");

//db
var connection = mysql.createConnection({
    host: "127.0.0.1",
    user: "root",
    password: "vajicko",
    database: "user",
    multipleStatements: true
});
connection.connect((err) => {
    if (!err) {
        console.log("DB connected");
    } else {
        console.log("DB connection failed. " + err)
    }
});

//passport.js setup
const customFields = {
    usernameField:"uname",
    passwordField:"pw",
};

//pics
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/post-pictures');
    },
    filename: function (req, file, cb) {
        const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
        cb(null, file.fieldname + '-' + uniqueSuffix + '.' + file.originalname.split('.').pop());
    }
});
const upload = multer({ storage: storage });


const verifyCallback = (username, pass, done) => {
    connection.query(
        "SELECT * FROM users WHERE username = ?",
        [username],
        function (error, results) {
            if (error) return done(error);

            if (results.length === 0) {
                return done(null, false);
            }
            const isValid = validPassword(
                pass,
                results[0].hash,
                results[0].salt
            );
            user = {
                id: results[0].id,
                username: results[0].username,
                hash: results[0].hash,
                salt: results[0].salt,
            };
            if (isValid) {
                return done(null, user);
            } else {
                return done(null, false);
            }
        }
    );
};

const strategy = new LocalStrategy(customFields,verifyCallback);
passport.use(strategy);

passport.serializeUser((user, done) => {
    done(null,user.id)
});

passport.deserializeUser(function(userId,done) {
    connection.query("SELECT * FROM users where id = ?",[userId], function(error, results) {
        done(null, results[0]);
    });
});

function validPassword(password,hash,salt)
{
    var hashVerify = crypto.pbkdf2Sync(password, salt, 10000, 60, "sha512").toString("hex");
    return hash === hashVerify;
}

function genPassword(password)
{
    var salt=crypto.randomBytes(32).toString("hex");
    var genhash = crypto.pbkdf2Sync(password, salt, 10000, 60, "sha512").toString("hex");
    return {salt:salt, hash:genhash};
}

function isAuth(req, res, next)
{
    if(req.isAuthenticated())
    {
        next();
    }
    else
    {
        res.redirect("/login");
    }
}

function isAdmin(req, res, next)
{
    if(req.isAuthenticated() && req.user.isadmin === 1)
    {
        next();
    }
    else
    {
        res.redirect("/login");
    }
}

function userExists(req, res, next)
{
    connection.query("SELECT * FROM users WHERE username = ?", [req.body.uname], function(error, results) {
        if (error)
        {
            console.log("Error");
        }
        else if (results.length>0)
        {
            res.redirect("/userAlreadyExists")
        }
        else
        {
            next();
        }
    });
}

function deleteUser(req) {
    const userid = req.user.id;
    connection.query("SELECT id, picture FROM posts WHERE user_id = ?", [userid], function(error, posts) {
        if (error) {
            console.log("Chyba pri nacitavani prispevkov:", error);
            return;
        }

        const postIds = posts.map(post => post.id);

        posts.forEach(post => {
            if (post.picture) {
                const filename = post.picture;
                fs.unlink(__dirname + "/public" + filename, (unlinkError) => {
                    if (unlinkError) {
                        console.error("Chyba pri vymazavani obrazka:", unlinkError);
                        return;
                    }
                    console.log(`Obrazok ${filename} uspesne vymazany.`);
                });
            }
        });

        connection.query("DELETE FROM replies WHERE user_id = ?", [userid], function(error) {
            if (error) {
                console.log("Chyba pri vymazavani odpovedi:", error);
                return;
            }

            connection.query("DELETE FROM posts WHERE user_id = ?", [userid], function(error) {
                if (error) {
                    console.log("Chyba pri vymazavani prispevkov", error);
                    return;
                }

                connection.query("DELETE FROM private_messages WHERE sender_id = ? OR receiver_id = ?", [userid, userid], function(error) {
                    if (error) {
                        console.log("Chyba pri vymazavani sukromnych sprav:", error);
                        return;
                    }

                    connection.query("DELETE FROM users WHERE id = ?", [userid], function(error) {
                        if (error) {
                            console.log("Chyba pri vymazavani pouzivatela:", error);
                            return;
                        }
                        console.log("Pouzivatel uspesne vymazany.");
                    });
                });
            });
        });
    });
}


function changeUsername(req) {
    connection.query("UPDATE users SET username = ? WHERE username = ?", [req.body.uname, req.user.username], function(error) {
        if (error)
        {
            console.log("Error");
        }
    });
}

function getUsername(req) {
    if (req.user === undefined) {
        return "Neprihlásený";
    } else {
        return req.user.username;
    }
}

app.get("/", (req, res) => {
    const topics = ["php", "aspnet", "javascript", "java", "python", "ruby", "other"];
    const counts = {};

    const fetchCounts = (index) => {
        if (index === topics.length) {
            res.render("index", { name: sanitizeHtml(getUsername(req)), counts });
        } else {
            const topic = topics[index];

            connection.query(`SELECT COUNT(*) AS total_${topic}_posts FROM posts WHERE topic = ?`, [topic], (postError, postResult) => {
                if (postError) {
                    console.error("Chyba pri získavaní počtu príspevkov:", postError);
                    return res.status(500).send("Internal Server Error");
                }

                counts[`${topic}_posts`] = postResult[0][`total_${topic}_posts`];

                connection.query(`SELECT COUNT(*) AS total_${topic}_replies FROM replies INNER JOIN posts ON replies.post_id = posts.id WHERE posts.topic = ?`, [topic], (replyError, replyResult) => {
                    if (replyError) {
                        console.error("Chyba pri získavaní počtu odpovedí:", replyError);
                        return res.status(500).send("Internal Server Error");
                    }

                    counts[`${topic}_replies`] = replyResult[0][`total_${topic}_replies`];

                    fetchCounts(index + 1);
                });
            });
        }
    };

    fetchCounts(0);
});

app.get("/rules", (req, res) => {
    res.render("rules", {name: sanitizeHtml(getUsername(req)) });
})

app.get("/register", (req, res) => {
    res.render("register", {name: sanitizeHtml(getUsername(req)) });
})

app.get("/login", (req, res) => {
    res.render("login", {name: sanitizeHtml(getUsername(req))});
})

app.get("/logout", (req, res) => {
    res.clearCookie("session_cookie_name")
    req.logout(function() {
        req.session.destroy(function () {
            res.redirect("/");
        });
    });
});

app.get("/loginFailure", (req, res) => {
    res.render("loginFailure", {name: sanitizeHtml(getUsername(req)) });
});

app.post("/register", userExists, (req, res) => {
    const saltHash = genPassword(req.body.pw);
    const salt = saltHash.salt;
    const hash = saltHash.hash;

    connection.query(
        "INSERT INTO users(username, hash, salt, isAdmin) VALUES (?, ?, ?, 0)",
        [req.body.uname, hash, salt],
        function (error) {
            if (error) {
                console.error("Pri registracii nastala chyba:", error);
            } else {
                console.log("Uspesna registracia");
            }
        }
    );

    res.redirect("/login");
});

app.post("/login",passport.authenticate("local",{failureRedirect:"/loginFailure",successRedirect:"/"}));

app.get("/userSettings",isAuth,(req, res) => {
    res.render("userSettings", {name: sanitizeHtml(getUsername(req))});
});

app.get("/adminSettings",isAdmin,(req, res) => {
    res.send("Ste admin");
});

app.get("/notAuthorized", (req, res) => {
    res.render("notAuthorized", {name: sanitizeHtml(getUsername(req))});
});

app.get("/userAlreadyExists", (req, res) => {
    res.render("userAlreadyExists", {name: sanitizeHtml(getUsername(req))});
});

app.get("/deleteAccount", (req, res) => {
    deleteUser(req);
    res.clearCookie("session_cookie_name")
    req.logout(function() {
        req.session.destroy(function () {
            res.redirect("/");
        });
    });
})

app.get("/changeUsername", (req, res) => {
    res.render("changeUsername", {name: sanitizeHtml(getUsername(req)) });
})

app.post("/changeUsername",userExists, (req, res) => {
    changeUsername(req);
    res.clearCookie("session_cookie_name")
    req.logout(function() {
        req.session.destroy(function () {
            res.redirect("/");
        });
    });
})

//vytvaranie prispevkov
app.post("/createPost/:topic", isAuth, upload.single('postPicture'), (req, res) => {
    const content = req.body.content;
    const topic = req.params.topic;
    const picturePath = req.file ? req.file.path.replace('public', '') : null;

    if (!content) {
        return res.status(400).send("Je potrebné niečo zadať.");
    }

    connection.query(
        "INSERT INTO posts (user_id, content, topic, picture) VALUES (?, ?, ?, ?)",
        [req.user.id, content, topic, picturePath],
        (error) => {
            if (error) {
                console.error("Chyba pri vytvarani prispevku:", error);
                return res.status(500).send("Internal Server Error");
            }

            res.redirect(`/topic/${topic}`);
        }
    );
});

//zobrazenie prispevkov
app.get("/topic/:topic", (req, res) => {
    const topic = req.params.topic;
    const userId = req.user ? req.user.id : null

    connection.query(
        "SELECT username, content, created_at, posts.id, posts.user_id, posts.picture FROM posts INNER JOIN users ON posts.user_id = users.id WHERE topic = ?",
        [topic],
        (error, posts) => {
            if (error) {
                console.error("Chyba pri nacitavani prispevkov:", error);
                return res.status(500).send("Internal Server Error");
            }

            res.render("topic", { topic, userId, name: sanitizeHtml(getUsername(req)), posts });
        }
    );
});

//odpovede
app.post("/createReply/:postId", isAuth, (req, res) => {
    const postId = req.params.postId;
    const { content } = req.body;

    connection.query(
        "INSERT INTO replies (user_id, post_id, content) VALUES (?, ?, ?)",
        [req.user.id, postId, content],
        (error) => {
            if (error) {
                console.error("Chyba pri vytvarani odpovede:", error);
                return res.status(500).send("Internal Server Error");
            }

            res.redirect(`/replies/${postId}`);
        }
    );
});

app.get("/replies/:postId", (req, res) => {
    const postId = req.params.postId;
    const userId = req.user ? req.user.id : null

    connection.query(
        "SELECT p.id as post_id, p.user_id as post_user_id, p.content as post_content, p.created_at as post_created_at, p.picture as post_picture," +
        "r.id as reply_id, r.user_id as reply_user_id, r.content as reply_content, r.created_at as reply_created_at, " +
        "u1.username as post_username, u2.username as reply_username " +
        "FROM posts p " +
        "LEFT JOIN replies r ON p.id = r.post_id " +
        "INNER JOIN users u1 ON u1.id = p.user_id " +
        "LEFT JOIN users u2 ON u2.id = r.user_id " +
        "WHERE p.id = ?",
        [postId],
        (error, results) => {
            if (error) {
                console.error("Chyba:", error);
                return res.status(500).send("Internal Server Error");
            }

            const originalPost = {
                id: results[0].post_id,
                user_id: results[0].post_user_id,
                username: results[0].post_username,
                content: results[0].post_content,
                created_at: results[0].post_created_at,
                picture: results[0].post_picture,
            };

            const replies = results
                .filter(row => row.reply_id && row.reply_user_id && row.reply_content && row.reply_created_at)
                .map(row => ({
                    id: row.reply_id,
                    user_id: row.reply_user_id,
                    username: row.reply_username,
                    content: row.reply_content,
                    created_at: row.reply_created_at,
                }));

            res.render("replies", { name: sanitizeHtml(getUsername(req)), originalPost, replies, userId });
        }
    );
});

//sukromne spravy
app.post("/sendMessage", isAuth, (req, res) => {
    const { recipientName, message_content } = req.body;

    connection.query(
        "SELECT id FROM users WHERE username = ?",
        [recipientName],
        (error, results) => {
            if (error) {
                console.error("Chyba pri vyhladavani prijemcu:", error);
                return res.status(500).send("Internal Server Error");
            }

            if (results.length === 0) {
                return res.status(404).render("recipientNotFound", {name: sanitizeHtml(getUsername(req))});
            }

            const recipientId = results[0].id;

            connection.query(
                "INSERT INTO private_messages (sender_id, receiver_id, message_content) VALUES (?, ?, ?)",
                [req.user.id, recipientId, message_content],
                (insertError) => {
                    if (insertError) {
                        console.error("Chyba pri posielani spravy:", insertError);
                        return res.status(500).send("Internal Server Error");
                    }

                    res.redirect("/messages");
                }
            );
        }
    );
});

app.get("/messages", isAuth, (req, res) => {
    connection.query(
        "SELECT pm.message_content, pm.timestamp, u1.username AS sender_username, u2.username AS receiver_username " +
        "FROM private_messages pm " +
        "JOIN users u1 ON pm.sender_id = u1.id " +
        "JOIN users u2 ON pm.receiver_id = u2.id " +
        "WHERE pm.receiver_id = ? " +
        "ORDER BY pm.timestamp DESC",
        [req.user.id],
        (error, messages) => {
            if (error) {
                console.error("Chyba pri nacitavani sprav:", error);
                return res.status(500).send("Internal Server Error");
            }

            res.render("messages", { name: sanitizeHtml(getUsername(req)), messages });
        }
    );
});

//pouzite pri AJAX refresh
app.get("/fetchMessages", isAuth, (req, res) => {
    connection.query(
        "SELECT pm.message_content, pm.timestamp, u1.username AS sender_username, u2.username AS receiver_username " +
        "FROM private_messages pm " +
        "JOIN users u1 ON pm.sender_id = u1.id " +
        "JOIN users u2 ON pm.receiver_id = u2.id " +
        "WHERE pm.receiver_id = ? " +
        "ORDER BY pm.timestamp DESC",
        [req.user.id],
        (error, messages) => {
            if (error) {
                console.error("Chyba pri nacitavani sprav:", error);
                return res.status(500).json({ error: "Internal Server Error" });
            }

            res.render("_messages", { messages }, (renderError, html) => {
                if (renderError) {
                    console.error("Chyba pri renderovani:", renderError);
                    return res.status(500).json({ error: "Internal Server Error" });
                }

                res.send(html);
            });
        }
    );
});

//vyhladavanie
app.get("/searchPage", (req, res) => {
    res.render("searchPage", { name: sanitizeHtml(getUsername(req)) });
});
app.get("/search", (req, res) => {
    const searchTerm = req.query.q;

    const query = `
        SELECT content, 'príspevok' AS type, id as post_id FROM posts WHERE content LIKE ? 
        UNION 
        SELECT content, 'odpoveď' AS type, post_id FROM replies WHERE content LIKE ?
    `;

    connection.query(query, [`%${searchTerm}%`, `%${searchTerm}%`], (error, results) => {
        if (error) {
            console.error("Chyba pri vyhladavani:", error);
            return res.status(500).json({ error: "Internal Server Error" });
        }

        res.json(results);
    });
});

app.get("/stats", (req, res) => {
    connection.query("SELECT COUNT(*) AS total_posts FROM posts", (error, postResult) => {
        if (error) {
            console.error("Chyba:", error);
            return res.status(500).send("Internal Server Error");
        }

        const totalPosts = postResult[0].total_posts;

        connection.query("SELECT COUNT(*) AS total_replies FROM replies", (error, replyResult) => {
            if (error) {
                console.error("Chyba:", error);
                return res.status(500).send("Internal Server Error");
            }

            const totalReplies = replyResult[0].total_replies;

            connection.query("SELECT COUNT(*) AS total_users FROM users", (error, userResult) => {
                if (error) {
                    console.error("Chyba:", error);
                    return res.status(500).send("Internal Server Error");
                }

                const totalUsers = userResult[0].total_users;

                res.render("stats", { name: sanitizeHtml(getUsername(req)), totalPosts, totalReplies, totalUsers });
            });
        });
    });
});

//uprava prispevkov
app.get("/editPost/:postId", isAuth, (req, res) => {
    const postId = req.params.postId;

    connection.query("SELECT * FROM posts WHERE id = ?", [postId], (error, results) => {
        if (error) {
            console.error("Chyba pri nacitavani prispevku:", error);
            return res.status(500).send("Internal Server Error");
        }

        if (results.length === 0 || results[0].user_id !== req.user.id) {
            return res.status(403).send("Neautorizovany");
        }

        res.render("editPost", { post: results[0], name: sanitizeHtml(getUsername(req)) });
    });
});

app.post("/editPost/:postId", isAuth, upload.single('postPicture'), (req, res) => {
    const postId = req.params.postId;
    const { content } = req.body;
    let picturePath = null;

    if (req.file) {
        picturePath = '/post-pictures/' + req.file.filename;
    }

    connection.query("SELECT picture FROM posts WHERE id = ? AND user_id = ?", [postId, req.user.id], (error, results) => {
        if (error) {
            console.error("Chyba pri ziskavani starej cesty k obrazku:", error);
            return res.status(500).send("Internal Server Error");
        }

        let query = '';
        let params = [];
        if (picturePath) {
            query = "UPDATE posts SET content = ?, picture = ? WHERE id = ? AND user_id = ?";
            params = [content, picturePath, postId, req.user.id];

            const oldPicturePath = results[0].picture;
            if (oldPicturePath) {
                fs.unlinkSync(__dirname + '/public' + oldPicturePath);
            }
        } else {
            query = "UPDATE posts SET content = ? WHERE id = ? AND user_id = ?";
            params = [content, postId, req.user.id];
        }

        connection.query(query, params, (updateError) => {
            if (updateError) {
                console.error("Chyba pri zmene prispevku:", updateError);
                return res.status(500).send("Internal Server Error");
            }

            res.redirect(`/`);
        });
    });
});

//mazanie prispevkov
app.post("/deletePost/:postId", isAuth, (req, res) => {
    const postId = req.params.postId;

    connection.query("SELECT picture FROM posts WHERE id = ?", [postId], (selectError, results) => {
        if (selectError) {
            console.error("Chyba databazovej query:", selectError);
            return res.status(500).send("Internal Server Error");
        }

        const filename = results[0].picture;

        // Delete the image file from the server
        fs.unlink(__dirname + "/public" + filename, (unlinkError) => {
            if (unlinkError) {
                console.error("Chyba pri vymazavani obrazka:", unlinkError);
                return res.status(500).send("Internal Server Error");
            }

            connection.query("DELETE FROM replies WHERE post_id = ?", [postId], (replyError) => {
                if (replyError) {
                    console.error("Chyba pri vymazavani odpovedi:", replyError);
                    return res.status(500).send("Internal Server Error");
                }

                connection.query("DELETE FROM posts WHERE id = ? AND user_id = ?", [postId, req.user.id], (postError) => {
                    if (postError) {
                        console.error("Chyba pri vymazavani prispevku:", postError);
                        return res.status(500).send("Internal Server Error");
                    }

                    res.redirect("/");
                });
            });
        });
    });
});

app.listen(3000, function () {
    console.log("Listening on port 3000")
})